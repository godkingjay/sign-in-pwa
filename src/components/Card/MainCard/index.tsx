import { forwardRef, ReactNode, Ref } from "react";
import {
	Card,
	CardContent,
	CardHeader,
	Divider,
	Typography,
} from "@mui/material";
import { useAppTheme } from "@/themes/hooks";

interface MainCardProps {
	border?: boolean;
	boxShadow?: boolean;
	children: React.ReactNode;
	content?: boolean;
	contentClass?: string;
	contentSX?: React.CSSProperties;
	darkTitle?: boolean;
	secondary?: ReactNode | string;
	shadow?: string;
	elevation?: number;
	sx?: React.CSSProperties;
	title?: ReactNode | string;
}

const headerSX = {
	"& .MuiCardHeader-action": { mr: 0 },
};

const MainCard = forwardRef(function MainCard(
	{
		border = true,
		boxShadow,
		children,
		content = true,
		contentClass = "",
		contentSX = {},
		darkTitle,
		secondary,
		shadow,
		elevation = 0,
		sx = {},
		title,
		...others
	}: MainCardProps,
	ref: Ref<HTMLDivElement>
) {
	const theme = useAppTheme();

	return (
		<Card
			ref={ref}
			{...others}
			sx={{
				border: border ? "1px solid" : "none",
				borderColor: theme.palette.primary.light,
				":hover": {
					boxShadow: boxShadow
						? shadow || "0 2px 14px 0 rgb(32 40 45 / 8%)"
						: "inherit",
				},
				...sx,
			}}
			elevation={elevation}
		>
			{title && (
				<CardHeader
					sx={headerSX}
					title={
						darkTitle ? <Typography variant="h3">{title}</Typography> : title
					}
					action={secondary}
				/>
			)}

			{title && <Divider />}

			{content && (
				<CardContent
					sx={contentSX}
					className={contentClass}
				>
					{children}
				</CardContent>
			)}
			{!content && children}
		</Card>
	);
});

export default MainCard;
