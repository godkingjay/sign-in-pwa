import { Typography, TypographyProps } from "@mui/material";
import React from "react";

type WavelyTextPropsProps = TypographyProps;

const WavelyText: React.FC<WavelyTextPropsProps> = (
	props: WavelyTextPropsProps
) => {
	return (
		<>
			<Typography
				{...props}
				component={"span"}
			>
				Wavely
			</Typography>
		</>
	);
};

export default WavelyText;
