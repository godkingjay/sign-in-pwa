import { useAppTheme } from "@/themes/hooks";
import { Box } from "@mui/material";
import { block } from "million/react";
import React from "react";

type LogoProps = {
	className?: string;
};

const WavelyLogo = block((props: LogoProps) => {
	const theme = useAppTheme();

	return (
		<Box
			className={`relative flex items-center justify-center ${props.className}`}
		>
			<svg
				xmlns="http://www.w3.org/2000/svg"
				fill="none"
				viewBox="0 0 1361 1361"
				className="w-full h-full"
			>
				<g>
					<rect
						x="0"
						y="0"
						fill="#ffffff00"
					/>
					<path
						d="M1304 680.5C1304 1024.85 1024.85 1304 680.5 1304 336.15 1304 57 1024.85 57 680.5 57 336.15 336.15 57 680.5 57 1024.85 57 1304 336.15 1304 680.5Z"
						stroke={theme.palette.primary.main}
						strokeWidth="96"
						strokeLinecap="round"
						strokeLinejoin="round"
						strokeMiterlimit="10"
						fill="none"
						fillRule="evenodd"
					/>
					<path
						d="M209 502C303.32 502 303.32 859 397.615 859 491.91 859 491.885 502 586.18 502 680.475 502 680.5 859 774.795 859 869.09 859 869.09 502 963.385 502 1057.68 502 1057.68 859 1152 859"
						stroke={theme.palette.primary.main}
						strokeWidth="96"
						strokeLinecap="round"
						strokeLinejoin="round"
						strokeMiterlimit="10"
						fill="none"
						fillRule="evenodd"
					/>
				</g>
			</svg>
		</Box>
	);
});

export default WavelyLogo;
