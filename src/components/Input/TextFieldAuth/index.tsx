import { FormGroup, TextField, TextFieldProps } from "@mui/material";
import { blue, red } from "@mui/material/colors";
import { block } from "million/react";
import React from "react";

type TextFieldAuthProps = TextFieldProps;

const TextFieldAuth = block((props: TextFieldAuthProps) => {
	return (
		<>
			<FormGroup
				sx={{
					width: "100%",
					display: "flex",
					flexDirection: "column",
				}}
			>
				<TextField
					InputLabelProps={{
						color: "primary",
						required: false,
						sx: {
							"&.Mui-focused": {
								fontWeight: "bold",
							},
						},
					}}
					{...props}
				/>
			</FormGroup>
		</>
	);
});

export default TextFieldAuth;
