"use client";

import { Box } from "@mui/material";
import React from "react";

type MainLayoutProps = {
	children: React.ReactNode;
};

const MainLayout: React.FC<MainLayoutProps> = (props) => {
	const { children } = props;

	return (
		<>
			<Box
				sx={{
					display: "flex",
					flexDirection: "column",
					flex: 1,
					height: "100%",
					maxHeight: "100vh",
					position: "relative",
				}}
			>
				{children}
			</Box>
		</>
	);
};

export default MainLayout;
