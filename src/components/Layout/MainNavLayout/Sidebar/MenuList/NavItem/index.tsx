import React from "react";

import {
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Typography,
	useMediaQuery,
} from "@mui/material";

import { MenuItemType } from "@/utils/menu-items/menu-item";
import { useAppTheme } from "@/themes/hooks";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { usePathname } from "next/navigation";
import { setActiveMenu, setSidebarOpen } from "@/redux/reducer/uiStateReducer";
import Link from "next/link";

type NavItemProps = {
	item: MenuItemType;
	level: number;
};

const NavItem: React.FC<NavItemProps> = ({ item, level }) => {
	const theme = useAppTheme();

	const dispatch = useAppDispatch();

	const pathname = usePathname();

	const uiState = useAppSelector((state) => state.uiState);

	const matchesSM = useMediaQuery(theme.breakpoints.down("sm"));

	const Icon = item.icon as React.ElementType;
	const itemIcon = <Icon size={20} />;

	let itemTarget = "_self";

	if (item.target) {
		itemTarget = "_blank";
	}

	const handleMenuItemClick = (id: string) => {
		dispatch(setActiveMenu(id));

		if (matchesSM) {
			dispatch(setSidebarOpen(false));
		}
	};

	React.useEffect(() => {
		const currentIndex = document.location.pathname
			.toString()
			.split("/")
			.findIndex((id) => id === item.id);

		if (currentIndex > -1) {
			dispatch(setActiveMenu(item.id));
		}
	}, [pathname]);

	return (
		<ListItemButton
			component={Link}
			href={
				uiState.activeMenu.findIndex((id) => id === item.id) > -1
					? "#"
					: item.url || "#"
			}
			target={itemTarget}
			disabled={item.disabled}
			sx={{
				borderRadius: `${uiState.customization.borderRadius}px`,
				mb: 0.5,
				alignItems: "flex-start",
				backgroundColor: level > 1 ? "transparent !important" : "inherit",
				py: level > 1 ? 1 : 1.25,
				pl: `${level * 24}px`,
			}}
			selected={uiState.activeMenu.findIndex((id) => id === item.id) > -1}
			onClick={() => handleMenuItemClick(item.id)}
		>
			<ListItemIcon sx={{ my: "auto", minWidth: !item?.icon ? 18 : 36 }}>
				{itemIcon}
			</ListItemIcon>
			<ListItemText
				primary={
					<Typography
						variant={
							uiState.activeMenu.findIndex((id) => id === item.id) > -1
								? "h5"
								: "body1"
						}
						color="inherit"
					>
						{item.title}
					</Typography>
				}
				secondary={
					item.caption && (
						<Typography
							variant="caption"
							sx={{ ...theme.typography.subMenuCaption }}
							display="block"
							gutterBottom
						>
							{item.caption}
						</Typography>
					)
				}
			/>
		</ListItemButton>
	);
};

export default NavItem;
