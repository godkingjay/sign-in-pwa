import { Box, Drawer, useMediaQuery } from "@mui/material";
import { drawerWidth } from "@/utils/constant";
import LogoSection from "../LogoSection";
import { BrowserView, MobileView } from "react-device-detect";
import MenuList from "./MenuList";
import { useAppTheme } from "@/themes/hooks";

type SidebarProps = {
	sidebarOpen: boolean;
	sidebarToggle: () => void;
	window?: Window;
};

const Sidebar: React.FC<SidebarProps> = ({
	sidebarOpen,
	sidebarToggle,
	window,
}) => {
	const theme = useAppTheme();
	const matchUpMd = useMediaQuery(theme.breakpoints.up("md"));

	const DrawerContent = (
		<>
			<Box sx={{ display: { xs: "block", md: "none" } }}>
				<Box sx={{ display: "flex", p: 2, mx: "auto" }}>
					<LogoSection />
				</Box>
			</Box>
			<BrowserView>
				<Box
					component="div"
					style={{
						height: !matchUpMd ? "calc(100vh - 56px)" : "calc(100vh - 88px)",
						paddingLeft: "16px",
						paddingRight: "16px",
					}}
				>
					<MenuList />
				</Box>
			</BrowserView>
			<MobileView>
				<Box sx={{ px: 2 }}>
					<MenuList />
				</Box>
			</MobileView>
		</>
	);

	const container =
		window !== undefined ? () => window.document.body : undefined;

	return (
		<Box
			component="nav"
			sx={{ flexShrink: { md: 0 }, width: matchUpMd ? drawerWidth : "auto" }}
			aria-label="mailbox folders"
		>
			<Drawer
				container={container}
				variant={matchUpMd ? "persistent" : "temporary"}
				anchor="left"
				open={sidebarOpen}
				onClose={sidebarToggle}
				sx={{
					"& .MuiDrawer-paper": {
						width: drawerWidth,
						background: theme.palette.background.default,
						color: theme.palette.text.primary,
						borderRight: "none",
						[theme.breakpoints.up("md")]: {
							top: "88px",
						},
					},
				}}
				ModalProps={{ keepMounted: true }}
				color="inherit"
			>
				{DrawerContent}
			</Drawer>
		</Box>
	);
};

export default Sidebar;
