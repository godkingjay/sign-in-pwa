import { Avatar, Box, ButtonBase } from "@mui/material";
import { HiMenuAlt2 } from "react-icons/hi";

import { useAppTheme } from "@/themes/hooks";
import LogoSection from "../LogoSection";
import SearchSection from "./SearchSection";
import ProfileSection from "./ProfileSection";

type HeaderProps = {
	handleSideBarToggle: () => void;
};

const Header: React.FC<HeaderProps> = ({ handleSideBarToggle }) => {
	const theme = useAppTheme();

	return (
		<>
			<Box
				sx={{
					width: 228,
					display: "flex",
					alignItems: "center",
					[theme.breakpoints.down("md")]: {
						width: "auto",
					},
				}}
			>
				<Box
					component="span"
					sx={{
						display: {
							xs: "none",
							md: "block",
						},
						flexGrow: 1,
					}}
				>
					<LogoSection />
				</Box>
				<ButtonBase
					sx={{
						width: 36,
						height: 36,
						borderRadius: "12px",
						overflow: "hidden",
						aspectRatio: "1/1",
					}}
				>
					<Avatar
						variant="rounded"
						sx={{
							...theme.typography.commonAvatar,
							...theme.typography.mediumAvatar,
							transition: "all .2s ease-in-out",
							background: theme.palette.secondary.light,
							color: theme.palette.secondary.dark,
							"&:hover": {
								background: theme.palette.secondary.dark,
								color: theme.palette.secondary.light,
							},
						}}
						onClick={handleSideBarToggle}
						color="inherit"
					>
						<HiMenuAlt2 size={20} />
					</Avatar>
				</ButtonBase>
			</Box>

			<SearchSection />

			<Box sx={{ flexGrow: 1 }} />
			<Box sx={{ flexGrow: 1 }} />

			<ProfileSection />
		</>
	);
};

export default Header;
