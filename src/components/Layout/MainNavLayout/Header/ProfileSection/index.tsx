import React, { useState, useRef, useEffect } from "react";

import {
	Avatar,
	Box,
	Card,
	CardContent,
	Chip,
	ClickAwayListener,
	Divider,
	Grid,
	List,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	Paper,
	Popper,
	Stack,
	Switch,
	Typography,
} from "@mui/material";

import Transitions from "@/components/UIExtender/Transitions";
import { useAppTheme } from "@/themes/hooks";
import { useAppSelector } from "@/redux/hooks";
import { useRouter } from "next/navigation";
import { IoLogOutOutline, IoSettingsOutline } from "react-icons/io5";
import { AiOutlineBell } from "react-icons/ai";
import MainCard from "@/components/Card/MainCard";

const ProfileSection = () => {
	const theme = useAppTheme();

	const uiState = useAppSelector((state) => state.uiState);

	const router = useRouter();

	const [notification, setNotification] = useState(false);
	const [selectedIndex, setSelectedIndex] = useState(-1);
	const [open, setOpen] = useState(false);

	const anchorRef = useRef<HTMLDivElement>(null);

	const handleLogout = async () => {
		router.push("/");
	};

	const handleClose = (
		event: MouseEvent | TouchEvent | React.MouseEvent<HTMLDivElement, MouseEvent>
	) => {
		if (
			anchorRef.current &&
			anchorRef.current.contains(event.target as HTMLDivElement)
		) {
			return;
		}
		setOpen(false);
	};

	const handleListItemClick = (
		event: React.MouseEvent<HTMLDivElement, MouseEvent>,
		index: number,
		route: string = ""
	) => {
		setSelectedIndex(index);
		handleClose(event);

		if (route && route !== "") {
			router.push(route);
		}
	};
	const handleToggle = () => {
		setOpen((prevOpen) => !prevOpen);
	};

	const prevOpen = useRef(open);

	useEffect(() => {
		if (prevOpen.current === true && open === false) {
			anchorRef.current?.focus();
		}

		prevOpen.current = open;
	}, [open]);

	return (
		<>
			<Chip
				sx={{
					height: "48px",
					alignItems: "center",
					borderRadius: "27px",
					transition: "all .2s ease-in-out",
					borderColor: theme.palette.primary.light,
					backgroundColor: theme.palette.primary.light,
					'&[aria-controls="menu-list-grow"], &:hover': {
						borderColor: theme.palette.primary.main,
						background: `${theme.palette.primary.main}!important`,
						color: theme.palette.primary.light,
						"& svg": {
							stroke: theme.palette.primary.light,
						},
					},
					"& .MuiChip-label": {
						lineHeight: 0,
					},
				}}
				icon={
					<Avatar
						src={"images/users/user0001.jpg"}
						sx={{
							...theme.typography.mediumAvatar,
							margin: "8px 0 8px 8px !important",
							cursor: "pointer",
						}}
						ref={anchorRef}
						aria-controls={open ? "menu-list-grow" : undefined}
						aria-haspopup="true"
						color="inherit"
					/>
				}
				label={
					<IoSettingsOutline
						color={theme.palette.primary.main}
						size={20}
					/>
				}
				variant="outlined"
				ref={anchorRef}
				aria-controls={open ? "menu-list-grow" : undefined}
				aria-haspopup="true"
				onClick={handleToggle}
				color="primary"
			/>
			<Popper
				placement="bottom-end"
				open={open}
				anchorEl={anchorRef.current}
				role={undefined}
				transition
				disablePortal
				popperOptions={{
					modifiers: [
						{
							name: "offset",
							options: {
								offset: [0, 14],
							},
						},
					],
				}}
			>
				{({ TransitionProps }) => (
					<Transitions
						in={open}
						position="top-right"
						{...TransitionProps}
					>
						<Paper>
							<ClickAwayListener onClickAway={handleClose}>
								<MainCard
									border={false}
									elevation={16}
									content={false}
									boxShadow
									shadow={theme.shadows[16]}
									sx={{
										display: "flex",
										flexDirection: "column",
										maxHeight: "calc(100vh - 88px)",
									}}
								>
									<Box sx={{ p: 2 }}>
										<Stack>
											<Stack
												direction="row"
												spacing={0.5}
												alignItems="center"
											>
												<Typography variant="h4">Good Morning,</Typography>
												<Typography
													component="span"
													variant="h4"
													sx={{ fontWeight: 400 }}
												>
													Jarrian Vince Gojar
												</Typography>
											</Stack>
											<Typography variant="subtitle2">Web Developer</Typography>
										</Stack>
									</Box>
									<Divider sx={{ mx: 2 }} />
									<Box
										className="scroll-y-style"
										sx={{
											overflowX: "hidden",
										}}
									>
										<Box
											sx={{
												p: 2,
												pt: 0,
											}}
										>
											<Card
												sx={{
													bgcolor: theme.palette.primary.light,
													my: 2,
												}}
											>
												<CardContent>
													<Grid
														container
														spacing={3}
														direction="column"
													>
														<Grid item>
															<Grid
																item
																container
																alignItems="center"
																justifyContent="space-between"
															>
																<Grid item>
																	<AiOutlineBell
																		size={20}
																		color={theme.palette.grey["700"]}
																	/>
																</Grid>
																<Grid item>
																	<Typography variant="subtitle1">
																		Allow Notifications
																	</Typography>
																</Grid>
																<Grid item>
																	<Switch
																		checked={notification}
																		onChange={(e) =>
																			setNotification(e.target.checked)
																		}
																		name="sdm"
																		size="small"
																	/>
																</Grid>
															</Grid>
														</Grid>
													</Grid>
												</CardContent>
											</Card>
											<Divider />
											<List
												component="nav"
												sx={{
													width: "100%",
													maxWidth: 350,
													minWidth: 300,
													backgroundColor: theme.palette.background.paper,
													borderRadius: "10px",
													[theme.breakpoints.down("md")]: {
														minWidth: "100%",
													},
													"& .MuiListItemButton-root": {
														mt: 0.5,
													},
												}}
											>
												<ListItemButton
													sx={{
														borderRadius: `${uiState.customization.borderRadius}px`,
													}}
													selected={selectedIndex === 0}
													onClick={(event) => handleListItemClick(event, 0, "#")}
												>
													<ListItemIcon>
														<IoSettingsOutline size={20} />
													</ListItemIcon>
													<ListItemText
														primary={
															<Typography variant="body2">
																Account Settings
															</Typography>
														}
													/>
												</ListItemButton>
												<ListItemButton
													sx={{
														borderRadius: `${uiState.customization.borderRadius}px`,
													}}
													selected={selectedIndex === 4}
													onClick={handleLogout}
												>
													<ListItemIcon>
														<IoLogOutOutline size={20} />
													</ListItemIcon>
													<ListItemText
														primary={
															<Typography variant="body2">Logout</Typography>
														}
													/>
												</ListItemButton>
											</List>
										</Box>
									</Box>
								</MainCard>
							</ClickAwayListener>
						</Paper>
					</Transitions>
				)}
			</Popper>
		</>
	);
};

export default ProfileSection;
