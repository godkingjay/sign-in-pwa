import { useAppTheme } from "@/themes/hooks";
import { AppTheme } from "@/themes/themeTypes";
import {
	Avatar,
	Box,
	InputAdornment,
	OutlinedInput,
	Popper,
	styled,
} from "@mui/material";
import { ButtonBase, Card, Grid } from "@mui/material";
import { AiOutlineSearch } from "react-icons/ai";
import React from "react";
import {
	PopupState as PopupStateType,
	bindPopper,
	bindToggle,
} from "material-ui-popup-state/hooks";
import { HiOutlineAdjustmentsHorizontal } from "react-icons/hi2";
import { IoCloseOutline } from "react-icons/io5";
import Transitions from "@/components/UIExtender/Transitions";
import PopupState from "material-ui-popup-state";

type PopperStyleProps = {
	theme: AppTheme;
};

const PopperStyle = styled(Popper, {
	shouldForwardProp: (prop) => true,
})<PopperStyleProps>(({ theme }) => ({
	zIndex: 1100,
	width: "99%",
	top: "-55px !important",
	padding: "0 12px",
	[theme.breakpoints.down("sm")]: {
		padding: "0 10px",
	},
}));

type OutlineInputStyleProps = {
	theme: AppTheme;
};

const OutlineInputStyle = styled(OutlinedInput, {
	shouldForwardProp: (prop) => true,
})<OutlineInputStyleProps>(({ theme }) => ({
	width: 434,
	marginLeft: 16,
	paddingLeft: 16,
	paddingRight: 16,
	"& input": {
		background: "transparent !important",
		paddingLeft: "4px !important",
	},
	[theme.breakpoints.down("lg")]: {
		width: 250,
	},
	[theme.breakpoints.down("md")]: {
		width: "100%",
		marginLeft: 4,
		background: "#fff",
	},
}));

type HeaderAvatarStyleProps = {
	theme: AppTheme;
};

const HeaderAvatarStyle = styled(Avatar, {
	shouldForwardProp: (prop) => true,
})<HeaderAvatarStyleProps>(({ theme }) => ({
	...theme.typography.commonAvatar,
	...theme.typography.mediumAvatar,
	background: theme.palette.secondary.light,
	color: theme.palette.secondary.dark,
	"&:hover": {
		background: theme.palette.secondary.dark,
		color: theme.palette.secondary.light,
	},
}));

type MobileSearchProps = {
	value: string;
	setValue: React.Dispatch<React.SetStateAction<string>>;
	popupState: PopupStateType;
};

const MobileSearch: React.FC<MobileSearchProps> = ({
	value,
	setValue,
	popupState,
}) => {
	const theme = useAppTheme();

	return (
		<OutlineInputStyle
			theme={theme}
			id="input-search-header"
			value={value}
			onChange={(e) => setValue(e.target.value)}
			placeholder="Search"
			startAdornment={
				<InputAdornment position="start">
					<AiOutlineSearch
						size={20}
						color={theme.palette.grey[500]}
					/>
				</InputAdornment>
			}
			endAdornment={
				<InputAdornment position="end">
					<ButtonBase sx={{ borderRadius: "12px" }}>
						<HeaderAvatarStyle
							theme={theme}
							variant="rounded"
						>
							<HiOutlineAdjustmentsHorizontal size={20} />
						</HeaderAvatarStyle>
					</ButtonBase>
					<Box sx={{ ml: 2 }}>
						<ButtonBase sx={{ borderRadius: "12px" }}>
							<Avatar
								variant="rounded"
								sx={{
									...theme.typography.commonAvatar,
									...theme.typography.mediumAvatar,
									background: theme.palette.orange.light,
									color: theme.palette.orange.dark,
									"&:hover": {
										background: theme.palette.orange.dark,
										color: theme.palette.orange.light,
									},
								}}
								{...bindToggle(popupState)}
							>
								<IoCloseOutline size={20} />
							</Avatar>
						</ButtonBase>
					</Box>
				</InputAdornment>
			}
			aria-describedby="search-helper-text"
			inputProps={{ "aria-label": "weight" }}
		/>
	);
};

const SearchSection = () => {
	const theme = useAppTheme();
	const [value, setValue] = React.useState("");

	return (
		<>
			<Box sx={{ display: { xs: "block", md: "none" } }}>
				<PopupState
					variant="popper"
					popupId="demo-popup-popper"
				>
					{(popupState) => (
						<>
							<Box sx={{ ml: 2 }}>
								<ButtonBase sx={{ borderRadius: "12px" }}>
									<HeaderAvatarStyle
										theme={theme}
										variant="rounded"
										{...bindToggle(popupState)}
									>
										<AiOutlineSearch size={20} />
									</HeaderAvatarStyle>
								</ButtonBase>
							</Box>
							<PopperStyle
								theme={theme}
								{...bindPopper(popupState)}
								transition
							>
								{({ TransitionProps }) => (
									<>
										<Transitions
											type="zoom"
											{...TransitionProps}
											sx={{ transformOrigin: "center left" }}
										>
											<Card
												sx={{
													background: "#fff",
													[theme.breakpoints.down("sm")]: {
														border: 0,
														boxShadow: "none",
													},
												}}
											>
												<Box sx={{ p: 2 }}>
													<Grid
														container
														alignItems="center"
														justifyContent="space-between"
													>
														<Grid
															item
															xs
														>
															<MobileSearch
																value={value}
																setValue={setValue}
																popupState={popupState}
															/>
														</Grid>
													</Grid>
												</Box>
											</Card>
										</Transitions>
									</>
								)}
							</PopperStyle>
						</>
					)}
				</PopupState>
			</Box>
			<Box sx={{ display: { xs: "none", md: "block" } }}>
				<OutlineInputStyle
					theme={theme}
					id="input-search-header"
					value={value}
					onChange={(e) => setValue(e.target.value)}
					placeholder="Search"
					startAdornment={
						<InputAdornment position="start">
							<AiOutlineSearch
								size={20}
								color={theme.palette.grey[500]}
							/>
						</InputAdornment>
					}
					endAdornment={
						<InputAdornment position="end">
							<ButtonBase sx={{ borderRadius: "12px" }}>
								<HeaderAvatarStyle
									theme={theme}
									variant="rounded"
								>
									<HiOutlineAdjustmentsHorizontal size={20} />
								</HeaderAvatarStyle>
							</ButtonBase>
						</InputAdornment>
					}
					aria-describedby="search-helper-text"
					inputProps={{ "aria-label": "weight" }}
				/>
			</Box>
		</>
	);
};

export default SearchSection;
