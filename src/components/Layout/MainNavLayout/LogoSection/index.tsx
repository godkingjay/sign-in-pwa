import { ButtonBase } from "@mui/material";
import WavelyLogo from "@/components/Logo/WavelyLogo";
import Link from "next/link";
import { useAppTheme } from "@/themes/hooks";
import WavelyText from "@/components/Logo/WavelyText";

const LogoSection = () => {
	const theme = useAppTheme();

	return (
		<>
			<ButtonBase
				disableRipple
				component={Link}
				href="/overview"
				className="flex flex-row gap-2 h-12 justify-start"
			>
				<WavelyLogo className="h-8 w-8" />
				<WavelyText
					sx={{
						...theme.typography.h3,
						color: theme.palette.grey[800],
						textTransform: "uppercase",
					}}
				/>
			</ButtonBase>
		</>
	);
};

export default LogoSection;
