"use client";

import { AppBar, Box, Toolbar, styled, useMediaQuery } from "@mui/material";
import React from "react";
import { useAppDispatch, useAppSelector } from "@/redux/hooks";
import { block } from "million/react";
import { drawerWidth } from "@/utils/constant";
import { AppTheme } from "@/themes/themeTypes";
import { useAppTheme } from "@/themes/hooks";
import Header from "./Header";
import { setSidebarOpen } from "@/redux/reducer/uiStateReducer";
import Sidebar from "./Sidebar";

type MainProps = {
	theme: AppTheme;
	open: boolean;
};

const Main = styled("main", {
	shouldForwardProp: (prop) => prop !== "open",
})<MainProps>(({ theme, open }) => ({
	...theme.typography.mainContent,
	borderBottomLeftRadius: 0,
	borderBottomRightRadius: 0,
	transition: theme.transitions.create(
		"margin",
		open
			? {
					easing: theme.transitions.easing.easeOut,
					duration: theme.transitions.duration.enteringScreen,
			  }
			: {
					easing: theme.transitions.easing.sharp,
					duration: theme.transitions.duration.leavingScreen,
			  }
	),
	[theme.breakpoints.up("md")]: {
		marginLeft: open ? 0 : -(drawerWidth - 20),
		width: `calc(100% - ${drawerWidth}px)`,
	},
	[theme.breakpoints.down("md")]: {
		marginLeft: "20px",
		width: `calc(100% - ${drawerWidth}px)`,
		padding: "16px",
	},
	[theme.breakpoints.down("sm")]: {
		marginLeft: "10px",
		width: `calc(100% - ${drawerWidth}px)`,
		padding: "16px",
		marginRight: "10px",
	},
}));

type MainNavLayoutProps = {
	children: React.ReactNode;
};

const MainNavLayout: React.FC<MainNavLayoutProps> = block((props) => {
	const theme = useAppTheme();
	const matchDownMd = useMediaQuery(theme.breakpoints.down("md"));

	const { sidebarOpen } = useAppSelector((state) => state.uiState);
	const dispatch = useAppDispatch();

	const handleSideBarToggle = () => {
		dispatch(setSidebarOpen(!sidebarOpen));
	};

	return (
		<>
			<Box className="flex">
				<AppBar
					enableColorOnDark
					position="fixed"
					color="inherit"
					elevation={0}
					sx={{
						bgcolor: theme.palette.background.default,
						transition: sidebarOpen ? theme.transitions.create("width") : "none",
					}}
				>
					<Toolbar>
						<Header handleSideBarToggle={handleSideBarToggle} />
					</Toolbar>
				</AppBar>

				<Sidebar
					sidebarOpen={!matchDownMd ? sidebarOpen : !sidebarOpen}
					sidebarToggle={handleSideBarToggle}
				/>

				<Main
					theme={theme}
					open={sidebarOpen}
				>
					{props.children}
				</Main>
			</Box>
		</>
	);
});

export default MainNavLayout;
