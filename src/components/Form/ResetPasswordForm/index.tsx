"use client";

import * as React from "react";
import { block } from "million/react";
import { Box, InputAdornment, Link, Paper, Typography } from "@mui/material";
import { HiOutlineMail } from "react-icons/hi";
import WavelyLogo from "../../Logo/WavelyLogo";
import { rgxEmailAddress } from "@/utils/regex";
import { LoadingButton } from "@mui/lab";
import TextFieldAuth from "../../Input/TextFieldAuth";
import WavelyText from "@/components/Logo/WavelyText";
import { useAppTheme } from "@/themes/hooks";
import NextLink from "next/link";

const ResetPasswordForm = block(() => {
	const [formLoading, setFormLoading] = React.useState(false);

	const theme = useAppTheme();

	const [resetPasswordSent, setResetPasswordSent] = React.useState(false);

	const [resetPasswordForm, setResetPasswordForm] = React.useState({
		email: "",
	});

	const [resetPasswordFormError, setResetPasswordFormError] = React.useState({
		email: false,
	});

	const handleInputFieldTextChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		const { name, value } = event.target;

		setResetPasswordForm((prev) => ({
			...prev,
			[name]: value,
		}));

		setResetPasswordFormError((prev) => ({
			...prev,
			[name]: false,
		}));
	};

	const handleSignUpFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		setFormLoading(true);

		const formError = {
			email: !rgxEmailAddress.test(resetPasswordForm.email),
		};

		setResetPasswordFormError((prev) => ({
			...prev,
			email: formError.email,
		}));

		setTimeout(() => {
			setFormLoading(false);
			setResetPasswordSent(true);
		}, 1000);
	};

	return (
		<>
			<Box className="flex-1 w-full flex flex-row gap-0">
				<Box
					className="flex-1 hidden xs:flex"
					sx={{
						backgroundImage:
							"url(https://source.unsplash.com/random?wallpapers)",
						backgroundRepeat: "no-repeat",
						backgroundColor: (t) =>
							t.palette.mode === "light"
								? t.palette.grey[50]
								: t.palette.grey[900],
						backgroundSize: "cover",
						backgroundPosition: "center",
					}}
				/>
				<Box
					component={Paper}
					elevation={6}
					className="w-full max-w-xl"
					square
				>
					<Box className="h-full w-full flex-1 flex flex-col items-center justify-center px-12 py-16 gap-4">
						<Box className="flex items-center justify-center flex-col">
							<WavelyLogo className="w-24 h-24" />
						</Box>
						<Box className="w-full flex flex-col gap-8 max-w-sm border rounded-md items-center">
							<Box className="flex flex-col items-center text-gray-700">
								<Typography className="font-bold text-center text-2xl md:text-3xl">
									Reset{" "}
									<WavelyText
										sx={{
											color: theme.palette.primary.main,
											fontSize: "inherit",
											display: "inline",
										}}
										variant="h1"
									/>{" "}
									Password
								</Typography>
							</Box>
							<div className="w-full h-[1px] bg-gray-100"></div>
							{resetPasswordSent ? (
								<>
									<Box className="flex flex-col gap-2 p-8 border-2 border-[#2962ff] border-dashed items-center justify-center rounded-lg">
										<Box className="h-12 w-12 text-[#2962ff]">
											<HiOutlineMail className="h-full w-full stroke-[1px]" />
										</Box>
										<p className="text-center text-sm">
											An email has been sent to{" "}
											<a
												href={`mailto:${resetPasswordForm.email}`}
												className="font-bold text-base text-blue-500"
											>
												{resetPasswordForm.email}
											</a>
										</p>
									</Box>
								</>
							) : (
								<>
									<Box
										component="form"
										className="w-full flex flex-col gap-6"
										onSubmit={handleSignUpFormSubmit}
									>
										<TextFieldAuth
											id="email"
											name="email"
											label="Email"
											type="email"
											error={resetPasswordFormError.email}
											value={resetPasswordForm.email}
											helperText={
												resetPasswordFormError.email
													? "Please enter a valid email address."
													: null
											}
											InputProps={{
												endAdornment: (
													<InputAdornment position="end">
														<HiOutlineMail className="h-10 w-10 p-2 stroke-[1.5px]" />
													</InputAdornment>
												),
											}}
											required
											onChange={handleInputFieldTextChange}
										/>
										<LoadingButton
											variant="contained"
											size="large"
											type="submit"
											loading={formLoading}
										>
											Reset Password
										</LoadingButton>
									</Box>
								</>
							)}
							<Box className="w-full flex flex-row flex-wrap text-center justify-between text-sm">
								<Link
									component={NextLink}
									className="text-blue-500 underline"
									href="/"
								>
									Sign In
								</Link>
								<Link
									component={NextLink}
									className="text-blue-500 underline"
									href="/register"
								>
									Create an Account
								</Link>
							</Box>
						</Box>
					</Box>
				</Box>
			</Box>
		</>
	);
});

export default ResetPasswordForm;
