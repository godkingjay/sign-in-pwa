"use client";

import * as React from "react";
import { block } from "million/react";
import {
	Box,
	IconButton,
	InputAdornment,
	Link,
	Paper,
	Typography,
} from "@mui/material";
import { HiOutlineEye, HiOutlineEyeSlash } from "react-icons/hi2";
import { HiOutlineMail } from "react-icons/hi";
import WavelyLogo from "../../Logo/WavelyLogo";
import { rgxEmailAddress, rgxPassword } from "@/utils/regex";
import { useRouter } from "next/navigation";
import { LoadingButton } from "@mui/lab";
import TextFieldAuth from "../../Input/TextFieldAuth";
import WavelyText from "@/components/Logo/WavelyText";
import { useAppTheme } from "@/themes/hooks";
import NextLink from "next/link";

const SignUpForm = block(() => {
	const router = useRouter();

	const theme = useAppTheme();

	const [formLoading, setFormLoading] = React.useState(false);

	const [signUpForm, setSignUpForm] = React.useState({
		email: "",
		password: "",
		repeatPassword: "",
	});

	const [signUpFormError, setSignUpFormError] = React.useState({
		email: false,
		password: false,
		repeatPassword: false,
	});

	const [showPassword, setShowPassword] = React.useState({
		password: false,
		repeatPassword: false,
	});

	const handleClickShowPassword = (type: "password" | "repeatPassword") => {
		switch (type) {
			case "password": {
				setShowPassword((prev) => ({
					...prev,
					password: !prev.password,
				}));

				break;
			}

			case "repeatPassword": {
				setShowPassword((prev) => ({
					...prev,
					repeatPassword: !prev.repeatPassword,
				}));

				break;
			}

			default: {
				break;
			}
		}
	};

	const handleMouseDownShowPassword = (type: "password" | "repeatPassword") => {
		switch (type) {
			case "password": {
				setShowPassword((prev) => ({
					...prev,
					password: !prev.password,
				}));

				break;
			}

			case "repeatPassword": {
				setShowPassword((prev) => ({
					...prev,
					repeatPassword: !prev.repeatPassword,
				}));

				break;
			}

			default: {
				break;
			}
		}
	};

	const handleInputFieldTextChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		const { name, value } = event.target;

		setSignUpForm((prev) => ({
			...prev,
			[name]: value,
		}));

		setSignUpFormError((prev) => ({
			...prev,
			[name]: false,
		}));
	};

	const handleSignUpFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		setFormLoading(true);

		const formError = {
			email: !rgxEmailAddress.test(signUpForm.email),
			password: !rgxPassword.test(signUpForm.password),
			repeatPassword: signUpForm.password !== signUpForm.repeatPassword,
		};

		setSignUpFormError((prev) => ({
			...prev,
			email: formError.email,
			password: formError.password,
			repeatPassword: formError.repeatPassword,
		}));

		if (formError.email || formError.password || formError.repeatPassword) {
			setFormLoading(false);

			return;
		}

		router.push("/overview");
	};

	return (
		<>
			<Box className="flex-1 w-full flex flex-row gap-0">
				<Box
					className="flex-1 hidden xs:flex"
					sx={{
						backgroundImage:
							"url(https://source.unsplash.com/random?wallpapers)",
						backgroundRepeat: "no-repeat",
						backgroundColor: (t) =>
							t.palette.mode === "light"
								? t.palette.grey[50]
								: t.palette.grey[900],
						backgroundSize: "cover",
						backgroundPosition: "center",
					}}
				/>
				<Box
					component={Paper}
					elevation={6}
					className="w-full max-w-xl"
					square
				>
					<Box className="h-full w-full flex-1 flex flex-col items-center justify-center px-12 py-16 gap-4 ">
						<Box className="flex items-center justify-center flex-col">
							<WavelyLogo className="w-24 h-24" />
						</Box>
						<Box className="w-full flex flex-col gap-8 max-w-sm border rounded-md items-center">
							<Box className="flex flex-col items-center text-gray-700">
								<Typography className="font-bold text-center text-2xl md:text-3xl">
									Create a{" "}
									<WavelyText
										sx={{
											color: theme.palette.primary.main,
											fontSize: "inherit",
											display: "inline",
										}}
										variant="h1"
									/>{" "}
									Account
								</Typography>
							</Box>
							<div className="w-full h-[1px] bg-gray-100"></div>
							<Box
								component="form"
								className="w-full flex flex-col gap-4"
								onSubmit={handleSignUpFormSubmit}
							>
								<TextFieldAuth
									id="email"
									name="email"
									label="Email"
									type="email"
									error={signUpFormError.email}
									value={signUpForm.email}
									helperText={
										signUpFormError.email
											? "Please enter a valid email address."
											: null
									}
									InputProps={{
										endAdornment: (
											<InputAdornment position="end">
												<HiOutlineMail className="h-10 w-10 p-2 stroke-[1.5px]" />
											</InputAdornment>
										),
									}}
									required
									onChange={handleInputFieldTextChange}
								/>
								<TextFieldAuth
									id="password"
									name="password"
									label="Password"
									type={showPassword.password ? "text" : "password"}
									error={
										showPassword.password ? false : signUpFormError.password
									}
									autoComplete="new-password"
									value={signUpForm.password}
									helperText={
										signUpFormError.password
											? "Password must be at least 8 characters long, contain uppercase letter, lowercase letter, number, or a special character (@, $, !, %, *, ?, &)."
											: null
									}
									InputProps={{
										endAdornment: (
											<InputAdornment position="end">
												<IconButton
													aria-label="toggle password visibility"
													onClick={() => handleClickShowPassword("password")}
													onMouseDown={() =>
														handleMouseDownShowPassword("password")
													}
												>
													{showPassword.password ? (
														<HiOutlineEye />
													) : (
														<HiOutlineEyeSlash />
													)}
												</IconButton>
											</InputAdornment>
										),
									}}
									FormHelperTextProps={{
										color: "error",
									}}
									required
									onChange={handleInputFieldTextChange}
								/>
								<TextFieldAuth
									id="repeatPassword"
									name="repeatPassword"
									label="Repeat Password"
									type={showPassword.repeatPassword ? "text" : "password"}
									error={
										showPassword.repeatPassword
											? false
											: signUpFormError.repeatPassword
									}
									autoComplete="new-password"
									value={signUpForm.repeatPassword}
									helperText={
										signUpFormError.repeatPassword
											? "Password does not match."
											: null
									}
									InputProps={{
										endAdornment: (
											<InputAdornment position="end">
												<IconButton
													aria-label="toggle password visibility"
													onClick={() =>
														handleClickShowPassword("repeatPassword")
													}
													onMouseDown={() =>
														handleMouseDownShowPassword("repeatPassword")
													}
												>
													{showPassword.repeatPassword ? (
														<HiOutlineEye />
													) : (
														<HiOutlineEyeSlash />
													)}
												</IconButton>
											</InputAdornment>
										),
									}}
									FormHelperTextProps={{
										color: "error",
									}}
									required
									onChange={handleInputFieldTextChange}
								/>
								<LoadingButton
									variant="contained"
									size="large"
									type="submit"
									color="primary"
									loading={formLoading}
								>
									Create Account
								</LoadingButton>
							</Box>
							<Box className="w-full text-center text-xs">
								<p>
									{"Already have an account?"}{" "}
									<Link
										component={NextLink}
										className="text-blue-500 underline"
										href="/"
									>
										Sign in instead.
									</Link>
								</p>
							</Box>
						</Box>
					</Box>
				</Box>
			</Box>
		</>
	);
});

export default SignUpForm;
