import { rgxEmailAddress, rgxPassword } from "@/utils/regex";
import {
	Box,
	Checkbox,
	FormControlLabel,
	FormGroup,
	IconButton,
	InputAdornment,
	Link,
	Typography,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { block } from "million/react";
import { useRouter } from "next/navigation";
import React from "react";
import { HiOutlineMail } from "react-icons/hi";
import { HiOutlineEye, HiOutlineEyeSlash } from "react-icons/hi2";
import TextFieldAuth from "../../Input/TextFieldAuth";
import WavelyText from "@/components/Logo/WavelyText";
import { useAppTheme } from "@/themes/hooks";
import NextLink from "next/link";

type SignInFormProps = {};

const SignInForm = block((props: SignInFormProps) => {
	const router = useRouter();

	const theme = useAppTheme();

	const [formLoading, setFormLoading] = React.useState(false);

	const [signInForm, setSignInForm] = React.useState({
		email: "",
		password: "",
	});

	const [signInFormError, setSignInFormError] = React.useState({
		email: false,
		password: false,
	});

	const [showPassword, setShowPassword] = React.useState(false);

	const handleClickShowPassword = () => {
		setShowPassword((prev) => !prev);
	};

	const handleMouseDownShowPassword = () => {
		setShowPassword((prev) => !prev);
	};

	const handleInputFieldTextChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		const { name, value } = event.target;

		setSignInForm((prev) => ({
			...prev,
			[name]: value,
		}));

		setSignInFormError((prev) => ({
			...prev,
			[name]: false,
		}));
	};

	const handleSignInFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		setFormLoading(true);

		setSignInFormError((prev) => ({
			...prev,
			email: !rgxEmailAddress.test(signInForm.email),
			password: !rgxPassword.test(signInForm.password),
		}));

		if (
			!rgxEmailAddress.test(signInForm.email) ||
			!rgxPassword.test(signInForm.password)
		) {
			setFormLoading(false);

			return;
		} else {
			router.push("/overview");
		}
	};

	return (
		<>
			<Box className="w-full flex flex-col gap-8">
				<Box className="flex flex-col items-center text-gray-700">
					<Typography className="font-bold text-center text-3xl md:text-4xl">
						Welcome to{" "}
						<WavelyText
							sx={{
								color: theme.palette.primary.main,
								fontSize: "inherit",
								display: "inline",
							}}
							variant="h1"
						/>
					</Typography>
				</Box>
				<div className="w-full h-[1px] bg-gray-100"></div>
				<Box
					component={"form"}
					className="w-full flex flex-col gap-4"
					onSubmit={handleSignInFormSubmit}
				>
					<TextFieldAuth
						id="email"
						name="email"
						label="Email"
						type="email"
						error={signInFormError.email}
						value={signInForm.email}
						helperText={
							signInFormError.email
								? "Please enter a valid email address."
								: null
						}
						onChange={handleInputFieldTextChange}
						InputProps={{
							endAdornment: (
								<InputAdornment position="end">
									<HiOutlineMail className="h-10 w-10 p-2 stroke-[1.5px]" />
								</InputAdornment>
							),
						}}
						required
					/>
					<TextFieldAuth
						id="password"
						name="password"
						label="Password"
						type={showPassword ? "text" : "password"}
						error={showPassword ? false : signInFormError.password}
						autoComplete="new-password"
						value={signInForm.password}
						helperText={
							signInFormError.password
								? "Password must be at least 8 characters long, contain uppercase letter, lowercase letter, number, or a special character (@, $, !, %, *, ?, &)."
								: null
						}
						InputProps={{
							endAdornment: (
								<InputAdornment position="end">
									<IconButton
										aria-label="toggle password visibility"
										onClick={handleClickShowPassword}
										onMouseDown={handleMouseDownShowPassword}
									>
										{showPassword ? <HiOutlineEye /> : <HiOutlineEyeSlash />}
									</IconButton>
								</InputAdornment>
							),
						}}
						FormHelperTextProps={{
							color: "error",
						}}
						required
						onChange={handleInputFieldTextChange}
					/>
					<Box className="w-full flex flex-row items-center flex-wrap gap-2">
						<FormGroup className="flex-1 flex flex-col justify-center text-gray-600">
							<FormControlLabel
								className="w-full"
								control={<Checkbox size="small" />}
								label="Remember Me"
								sx={{
									"& .MuiTypography-root": {
										fontSize: "0.875rem",
									},
								}}
							/>
						</FormGroup>
						<Box className="flex-1 text-right text-gray-500 text-sm">
							<Link href="/reset-password">Forgot Password?</Link>
						</Box>
					</Box>
					<LoadingButton
						variant="contained"
						size="large"
						type="submit"
						color="primary"
						loading={formLoading}
					>
						Sign In
					</LoadingButton>
				</Box>
				<Box className="w-full text-center text-xs">
					<p>
						{"Doesn't have an account?"}{" "}
						<Link
							component={NextLink}
							className="text-blue-500 underline"
							href="/register"
						>
							Create an account.
						</Link>
					</p>
				</Box>
			</Box>
		</>
	);
});

export default SignInForm;
