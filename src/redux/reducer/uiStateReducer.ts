import customization from "@/utils/customization";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { AppCustomization } from "../../themes/themeTypes";

type ReducerUIStateValueType = {
	sidebarOpen: boolean;
	activeMenu: string[];
	customization: AppCustomization;
};

const initialState: ReducerUIStateValueType = {
	sidebarOpen: false,
	activeMenu: [],
	customization: {
		borderRadius: customization.borderRadius,
		themeMode: "light",
	},
};

const uiStateSlice = createSlice({
	name: "materialUI",
	initialState,
	reducers: {
		setActiveMenu(state, action: PayloadAction<string>) {
			state.activeMenu = [action.payload];
		},
		setBorderRadius(
			state,
			action: PayloadAction<
				ReducerUIStateValueType["customization"]["borderRadius"]
			>
		) {
			state.customization.borderRadius = action.payload;
		},
		setSidebarOpen(
			state,
			action: PayloadAction<ReducerUIStateValueType["sidebarOpen"]>
		) {
			state.sidebarOpen = action.payload;
		},
		setThemeMode(
			state,
			action: PayloadAction<
				ReducerUIStateValueType["customization"]["themeMode"]
			>
		) {
			state.customization.themeMode = action.payload;
		},
	},
});

export const { setActiveMenu, setBorderRadius, setSidebarOpen, setThemeMode } =
	uiStateSlice.actions;

export default uiStateSlice.reducer;
