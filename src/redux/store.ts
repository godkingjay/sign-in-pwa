import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import uiStateReducer from "./reducer/uiStateReducer";

export const store = configureStore({
	reducer: {
		uiState: uiStateReducer,
	},
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	RootState,
	unknown,
	Action<string>
>;
