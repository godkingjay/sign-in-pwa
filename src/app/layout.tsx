import "@/styles/globals.scss";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { Suspense } from "react";
import Loading from "./loading";
import { CssBaseline, StyledEngineProvider } from "@mui/material";
import ReduxProvider from "@/redux/ReduxProvider";
import MainLayout from "@/components/Layout/MainLayout";
import MainThemeProvider from "@/themes/MainThemeProvider";

const fontInter = Inter({
	subsets: ["latin"],
	weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
});

export const metadata: Metadata = {
	title: "Wavely",
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
	return (
		<>
			<html lang="en">
				<head>
					<meta charSet="utf-8" />
					<link
						rel="icon"
						href="/wavely.svg"
					/>
				</head>
				<body className={fontInter.className}>
					<ReduxProvider>
						<StyledEngineProvider injectFirst>
							<MainThemeProvider>
								<Suspense fallback={<Loading />}>
									<CssBaseline />
									<MainLayout>{children}</MainLayout>
								</Suspense>
							</MainThemeProvider>
						</StyledEngineProvider>
					</ReduxProvider>
				</body>
			</html>
		</>
	);
}
