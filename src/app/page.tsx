"use client";

import { Box } from "@mui/material";
import WavelyLogo from "@/components/Logo/WavelyLogo";
import SignInForm from "@/components/Form/SignInForm";

type RootProps = {};

const Root: React.FC<RootProps> = (props: RootProps) => {
	return (
		<>
			<Box className="flex-1 px-12 py-16 flex flex-col items-center justify-center">
				<Box className="flex flex-col gap-4 max-w-md w-full border rounded-md p-8">
					<Box className="flex items-center justify-center flex-col">
						<WavelyLogo className="w-24 h-24" />
					</Box>
					<SignInForm />
				</Box>
			</Box>
		</>
	);
};

export default Root;
