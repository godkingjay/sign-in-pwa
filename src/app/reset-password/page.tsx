import ResetPasswordForm from "@/components/Form/ResetPasswordForm";
import { Metadata } from "next";
import React from "react";

type ResetPasswordPageProps = {};

export const metadata: Metadata = {
	title: "Reset Password | Wavely",
};
const ResetPasswordPage = (props: ResetPasswordPageProps) => {
	return (
		<>
			<ResetPasswordForm />
		</>
	);
};

export default ResetPasswordPage;
