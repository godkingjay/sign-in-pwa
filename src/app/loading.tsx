"use client";

import WavelyLogo from "@/components/Logo/WavelyLogo";
import { Box, Typography } from "@mui/material";
import { block } from "million/react";
import React from "react";

type LoadingProps = {};

const Loading = block((props: LoadingProps) => {
	return (
		<>
			<Box
				sx={{
					display: "flex",
					flexDirection: "column",
					flex: 1,
					gap: "16px",
					alignItems: "center",
					justifyContent: "center",
				}}
			>
				<Box
					sx={{
						width: "128px",
						height: "128px",
						animation: "pulse 1.5s infinite",
					}}
				>
					<WavelyLogo />
				</Box>
			</Box>
		</>
	);
});

export default Loading;
