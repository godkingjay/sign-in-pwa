import MainNavLayout from "@/components/Layout/MainNavLayout";
import { Metadata } from "next";
import React from "react";

export const metadata: Metadata = {
	title: "Home | Wavely",
};

type HomePageProps = {};

const HomePage = (props: HomePageProps) => {
	return (
		<>
			<MainNavLayout>
				<p>
					Lorem ipsum dolor sit amet consectetur, adipisicing elit. Unde earum
					est quasi facilis dolore distinctio dolorum nobis sit eos odio
					praesentium, iste placeat nesciunt officiis saepe dolorem eum voluptate
					eaque. Excepturi soluta, obcaecati sunt dolorum minima harum eum
					explicabo minus exercitationem qui recusandae perspiciatis
					necessitatibus incidunt illum maxime, id ad, cupiditate vitae nesciunt
					quia tempora pariatur assumenda vel quidem. Quisquam! Eum tempore,
					ullam, sequi libero velit neque temporibus ad cumque et odit tempora
					fuga ea repudiandae, eos alias laborum magni distinctio quasi in
					perspiciatis! Porro ipsa nesciunt iure nulla odit. Deleniti quibusdam
					omnis facilis! Iusto cupiditate quod ex harum. Quisquam numquam ullam
					magnam ipsa, consectetur optio autem reprehenderit, repudiandae at amet
					alias adipisci porro quis! Adipisci minus praesentium quam explicabo.
					Placeat exercitationem veritatis, ea perferendis esse id harum
					necessitatibus quasi perspiciatis sunt voluptate nesciunt non
					laudantium illum maiores ipsum! Incidunt nemo veniam quis quod at! Quos
					blanditiis ut error voluptatibus. In tempora magni voluptate dolore.
					Debitis molestiae ab assumenda repudiandae sunt eos quidem quos cumque
					atque sed, nobis tempora, laudantium, doloremque blanditiis voluptates
					ea maxime rerum vel. Labore, explicabo accusantium? Quaerat fugiat sunt
					repellat praesentium necessitatibus molestias quam, nostrum beatae
					obcaecati nihil amet totam consequuntur exercitationem asperiores
					facere temporibus odit sequi voluptate omnis? Nesciunt laborum aliquid
					at quaerat sint doloremque! Magnam similique totam laboriosam hic.
					Nulla voluptatum quod voluptate quo ipsa pariatur corrupti dolor
					doloremque, suscipit, culpa recusandae officiis cumque unde fuga iste,
					in vitae sequi velit. Rem, eius impedit. Aliquid quis dicta expedita
					reiciendis, quidem perferendis in, impedit asperiores atque totam est
					beatae officiis fuga nesciunt! Ex, odio impedit. Quasi tempore officia
					ullam perferendis id placeat ea optio nostrum. Nisi, veritatis numquam
					unde quaerat voluptates consequatur officiis beatae labore tempore
					inventore nihil minus libero eos magni eaque debitis. Ad veritatis
					placeat dolores suscipit velit enim repellendus repellat quidem
					commodi? Doloremque autem commodi voluptas delectus consequatur, quo
					voluptatem aperiam sequi eum enim optio placeat facilis qui dicta
					dignissimos repellat perspiciatis cupiditate natus consectetur, dolore
					quibusdam similique voluptatum exercitationem architecto. Deserunt?
					Excepturi, sed tempore! Ea dolorum sapiente aut quasi illo officiis sed
					nulla ipsa aperiam aspernatur dolore ad nobis, at nesciunt placeat
					ducimus vitae quam iusto iste autem maiores. Distinctio, quo. Harum
					possimus, voluptatem recusandae quibusdam sed consectetur illum.
					Consequuntur unde animi nisi eaque reiciendis maxime aspernatur quas
					commodi asperiores maiores pariatur veniam laudantium ad illum sint,
					dicta voluptatum blanditiis explicabo. Eum quod maxime quis labore
					obcaecati esse assumenda pariatur nulla tempore inventore quos sed
					similique debitis qui reiciendis amet architecto accusantium animi,
					quaerat quam commodi? Et quasi nulla temporibus vero. Aut unde in
					architecto nisi pariatur? Perspiciatis, voluptas alias? Reiciendis
					nulla minus quibusdam voluptatem suscipit ducimus eum nostrum quia
					corrupti quaerat expedita dicta ex eveniet facere doloremque neque,
					voluptates consequatur! Atque libero consequuntur illum fuga ab dolores
					laudantium autem, quo ullam quas expedita neque incidunt labore,
					delectus architecto ex similique! Voluptatibus nihil ea, beatae nemo
					iusto voluptate quibusdam consequatur laboriosam! Illum, neque iste,
					enim perferendis saepe, cum sapiente reiciendis cumque repellendus id
					officia. Perferendis repellat quo maxime facilis modi et vero
					temporibus cumque aspernatur! Ipsam facilis est dolore doloremque
					animi. Expedita aliquid omnis numquam tempore autem cupiditate ex non
					vitae necessitatibus cum molestiae aperiam inventore, nemo dignissimos
					animi nisi, cumque iusto veniam laudantium amet tempora magni. Ipsam ad
					totam delectus? Quam veniam pariatur totam? Itaque maxime et velit
					consequuntur ut repellat omnis dolore quam ullam officiis quo eius sint
					error nisi sit est voluptatem corrupti iusto, veritatis rerum
					accusantium nihil. Id eos aut a ullam voluptatibus pariatur beatae
					voluptatem doloribus dolorum. Illo, sequi! Necessitatibus quae
					molestiae, in explicabo obcaecati voluptatum delectus dolore. Quae
					veritatis asperiores praesentium velit possimus! Dignissimos,
					praesentium? Dignissimos a, officia eius animi ipsum facilis explicabo
					libero. Expedita libero ratione doloribus, delectus mollitia ab atque
					tempora cupiditate eos repellat asperiores! Aperiam eos ea unde rem
					deleniti! Corrupti, fugiat. Vero ipsa tempore soluta inventore eaque
					voluptates quae debitis ad, eligendi maiores, quas sit laudantium.
					Earum ut sunt dolor molestias excepturi deleniti eaque culpa sed.
					Molestiae optio nobis minima earum? Quos velit quia necessitatibus
					eaque tempora tenetur iusto quisquam, voluptas facilis nam! Deleniti
					sint amet sequi dolorem optio numquam est! Eaque exercitationem commodi
					perferendis sed libero a. Quaerat, aperiam fugiat. Iure, totam ex
					deserunt fugiat deleniti facere consequatur expedita corrupti
					laboriosam aut rem voluptatem similique in et ad impedit, vero, esse
					repellat consectetur sit fuga obcaecati enim? Dolorum, aspernatur
					vitae? Veritatis dolorum velit natus distinctio quibusdam ipsa alias
					laborum et tempora nulla, cum non debitis consectetur repellat voluptas
					eius saepe molestiae quisquam nobis, aperiam officia esse quaerat.
					Architecto, eos doloribus. Laborum saepe ea optio vel voluptatum a, sit
					molestiae itaque nulla illo fuga unde, facilis quibusdam libero sint
					error reprehenderit odit? Quo, nihil alias. Porro quod saepe nam
					incidunt aliquid. Mollitia temporibus culpa voluptatibus, voluptates
					repellat saepe expedita. At aspernatur vitae harum accusantium aut,
					consequatur ut officia error beatae sapiente maiores quam aperiam
					impedit optio ducimus a quibusdam molestiae nam? Velit sapiente eius
					totam? Eligendi quam ullam molestiae earum. Necessitatibus, tempora
					fugiat! Voluptatum, vel? Vero ab sunt quas exercitationem illum
					reprehenderit, numquam facere molestias ipsum quis magnam fugit quos
					similique? Porro rem vero, optio in harum obcaecati molestias. Ratione
					consectetur corporis in dolorum labore? Ipsum aliquid possimus quas
					dolorum consequuntur! Dolore laudantium natus iure, reprehenderit
					aliquam illum fugit distinctio sapiente? Assumenda nemo sunt maiores
					iusto nulla optio est molestias consequuntur, corporis harum ipsam
					quidem nobis porro! Officia repellat hic sit possimus? Aperiam placeat,
					totam fuga qui maiores possimus voluptates minima? Quidem autem
					provident esse quod, accusantium eum consequatur facere! Totam corporis
					atque suscipit esse dicta nobis fugiat dignissimos odio quibusdam
					rerum, aliquam unde a! Ipsum ratione architecto magni assumenda illo?
					Quasi voluptatum ullam sed architecto cum cumque non illum omnis nam
					nisi veritatis facilis incidunt, eum voluptatem quae dignissimos soluta
					error qui id consequatur! Est placeat quasi non nobis obcaecati?
					Sapiente aliquid maxime id quod ducimus sunt quidem reprehenderit a
					magnam aut laudantium libero, inventore in suscipit porro possimus
					ipsam commodi aperiam et est repudiandae. Quas nam assumenda repellat
					delectus! Nesciunt tenetur nihil a quibusdam reiciendis, omnis sit
					ducimus aliquam labore quia, laboriosam eius. Odit, accusamus est minus
					itaque nobis ea nesciunt suscipit laborum fugiat pariatur excepturi
					labore sapiente facilis. Quasi exercitationem fugiat quos deleniti
					repellendus atque doloribus recusandae nulla vero suscipit natus vitae
					consequuntur, ea nemo sapiente cumque, dolor soluta! Dignissimos,
					tempora quod! Vel error quo soluta facere reiciendis. Aut minus minima
					inventore nemo deleniti quaerat ipsam consequuntur quibusdam
					necessitatibus quisquam modi vero repellat iure quidem amet autem
					recusandae officia consequatur officiis a, aperiam nisi ab dicta.
					Perferendis, facere! Suscipit itaque obcaecati in dignissimos, modi,
					consectetur, illum fugiat odio magnam aut nostrum unde cupiditate
					exercitationem repellat nemo eius corrupti maxime vitae atque est
					ducimus commodi aspernatur! Itaque, laborum commodi? Minima dolorum
					quis consequuntur aspernatur voluptas distinctio. Ab voluptates
					excepturi voluptate qui quia tempora maiores, tenetur cumque
					perspiciatis odit aut asperiores! Nulla velit deserunt aliquam neque
					illo tempore quaerat nam. Labore facilis culpa, praesentium maxime
					quasi id ab facere neque architecto dolores soluta quia fuga aspernatur
					veniam cum nam, quod totam dolore, alias fugiat. Modi aut quae
					asperiores voluptatibus nulla? Incidunt voluptate corporis maiores
					placeat modi, praesentium in aperiam perspiciatis nam reprehenderit
					architecto cum dolorem veritatis repudiandae quis excepturi inventore!
					Sequi pariatur accusantium quasi consequatur fuga facere voluptate
					accusamus est. Sint eos unde eaque! Ab optio corrupti dolorem laborum
					accusamus sint consectetur voluptatem maiores asperiores earum. Quod
					saepe repudiandae tempora assumenda, exercitationem corporis quam ut
					cumque doloribus ipsum maiores adipisci. Iure illum nihil similique
					ratione recusandae sapiente iste, saepe nisi facilis odit blanditiis
					commodi quos aut nemo beatae quae quis aliquam voluptatem eos, fuga
					accusantium dicta suscipit delectus repudiandae! Sed? Recusandae, nobis
					omnis aut ipsum laborum rem, voluptatum rerum fugit, commodi doloribus
					iste totam. Perspiciatis molestias, voluptate illo ullam molestiae,
					laboriosam incidunt accusantium labore sapiente veniam sed alias.
					Impedit, blanditiis! In voluptate accusantium modi dolore libero eius
					blanditiis minima culpa pariatur suscipit tempore consequuntur
					provident harum laboriosam nam quia facilis, eligendi neque debitis
					iusto corrupti rem hic veritatis ad. Vel. Rerum, voluptas corrupti
					beatae totam sed odio exercitationem delectus expedita consequatur
					eligendi eius magni. Dolorem debitis aliquam dolore a, culpa nesciunt
					perspiciatis esse nihil id distinctio laboriosam ullam sequi omnis. Ex,
					repudiandae magni saepe at modi tempore libero quaerat deleniti unde
					iure error nam quisquam eum tenetur, quas voluptatem, adipisci
					laboriosam ipsum! Quos magnam provident aspernatur eveniet natus dicta
					est? Cupiditate est alias sit possimus. Rem placeat incidunt obcaecati
					eius tempore quos hic quis voluptas totam optio, enim sapiente
					consequuntur autem iste possimus velit in repellat. Exercitationem
					doloribus ipsum laboriosam. Quibusdam neque porro optio repellendus
					numquam molestias eaque nostrum velit similique totam, animi quas omnis
					quaerat, laboriosam illum beatae! Quas dolorem nostrum quod accusamus
					odit corrupti libero molestias harum iure. Facilis, et, praesentium
					voluptate repellendus ullam commodi nihil sed esse officia repellat
					soluta illo, vitae tenetur exercitationem. Ullam error facilis pariatur
					impedit eaque, iste fugiat commodi mollitia dolorem nemo modi. Commodi
					facilis iste quas voluptates totam voluptas quisquam, odit quis
					reprehenderit sed placeat doloremque. Libero, debitis quas, molestias
					esse sint recusandae sit, consectetur quidem dolorum vero nemo. Illo,
					ex doloribus.
				</p>
			</MainNavLayout>
		</>
	);
};

export default HomePage;
