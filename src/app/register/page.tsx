import SignUpForm from "@/components/Form/SignUpForm";
import { Metadata } from "next";
import React from "react";

export const metadata: Metadata = {
	title: "Sign Up | Wavely",
};

type SignUpFormProps = {};

const SignUpPage = (props: SignUpFormProps) => {
	return (
		<>
			<SignUpForm />
		</>
	);
};

export default SignUpPage;
