import {
	Palette,
	PaletteColor,
	PaletteColorOptions,
	PaletteOptions,
	Theme,
	ThemeOptions,
	TypographyStyle,
} from "@mui/material";

export type AppThemeColor = {
	paper: string;

	primaryLight: string;
	primaryMain: string;
	primaryDark: string;
	primary200: string;
	primary800: string;

	secondaryLight: string;
	secondaryMain: string;
	secondaryDark: string;
	secondary200: string;
	secondary800: string;

	successLight: string;
	success200: string;
	successMain: string;
	successDark: string;

	errorLight: string;
	errorMain: string;
	errorDark: string;

	orangeLight: string;
	orangeMain: string;
	orangeDark: string;

	warningLight: string;
	warningMain: string;
	warningDark: string;

	grey50: string;
	grey100: string;
	grey200: string;
	grey300: string;
	grey400: string;
	grey500: string;
	grey600: string;
	grey700: string;
	grey900: string;

	darkPaper: string;
	darkBackground: string;
	darkLevel1: string;
	darkLevel2: string;

	darkPrimaryLight: string;
	darkPrimaryMain: string;
	darkPrimaryDark: string;
	darkPrimary200: string;
	darkPrimary800: string;

	darkSecondaryLight: string;
	darkSecondaryMain: string;
	darkSecondaryDark: string;
	darkSecondary200: string;
	darkSecondary800: string;

	darkTextTitle: string;
	darkTextPrimary: string;
	darkTextSecondary: string;
};

export type AppThemePalette = PaletteOptions & {
	orange: Partial<PaletteColor>;
	dark: Partial<PaletteColor> & {
		800: string;
		900: string;
	};
	text: PaletteOptions["text"] & {
		dark: string;
		hint: string;
	};
};

export type AppThemeTypography = Partial<Theme["typography"]> & {
	customInput: TypographyStyle;
	mainContent: TypographyStyle;
	menuCaption: TypographyStyle;
	subMenuCaption: TypographyStyle;
	commonAvatar: TypographyStyle;
	smallAvatar: TypographyStyle;
	mediumAvatar: TypographyStyle;
	largeAvatar: TypographyStyle;
};

export type AppCustomization = {
	borderRadius: number;
	themeMode: Palette["mode"];
};

export type AppThemeOption = {
	colors: AppThemeColor;
	heading: string;
	paper: string;
	backgroundDefault: string;
	background: string;
	darkTextPrimary: string;
	darkTextSecondary: string;
	textDark: string;
	menuSelected: string;
	menuSelectedBack: string;
	divider: string;
	customization: AppCustomization;
};

export type AppTheme = Theme & {
	palette: AppThemePalette;
	typography: AppThemeTypography;
};
