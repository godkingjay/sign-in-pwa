"use client";

import { ThemeProvider } from "@mui/material";
import React from "react";
import { themeConfig } from "./theme";
import { useAppSelector } from "../redux/hooks";

type MainThemeProviderProps = {
	children: React.ReactNode;
};

const MainThemeProvider: React.FC<MainThemeProviderProps> = (props) => {
	const { customization } = useAppSelector((state) => state.uiState);

	return (
		<>
			<ThemeProvider theme={themeConfig(customization)}>
				{props.children}
			</ThemeProvider>
		</>
	);
};

export default MainThemeProvider;
