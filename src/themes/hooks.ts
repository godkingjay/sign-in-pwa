import { useTheme } from "@mui/material/styles";
import { AppTheme } from "./themeTypes";

export const useAppTheme = () => useTheme() as AppTheme;
