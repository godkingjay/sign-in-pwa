import { Theme, createTheme } from "@mui/material";
import { blue, green, red } from "@mui/material/colors";

import colorStyle from "@/styles/_themes-var.module.scss";
import { AppThemeOption, AppThemeColor, AppCustomization } from "./themeTypes";
import themeTypography from "./typography";
import themePalette from "./palette";
import componentStyleOverride from "./componentStyleOverride";

export const themeConfig = (customization: AppCustomization) => {
	const themeOption: AppThemeOption = {
		colors: colorStyle as AppThemeColor,
		heading: colorStyle.grey900,
		paper: colorStyle.paper,
		backgroundDefault: colorStyle.paper,
		background: colorStyle.primaryLight,
		darkTextPrimary: colorStyle.grey700,
		darkTextSecondary: colorStyle.grey500,
		textDark: colorStyle.grey900,
		menuSelected: colorStyle.secondaryDark,
		menuSelectedBack: colorStyle.secondaryLight,
		divider: colorStyle.grey200,
		customization: customization,
	};

	const themeConfiguration = createTheme({
		direction: "ltr",
		palette: themePalette(themeOption),
		mixins: {
			toolbar: {
				minHeight: "48px",
				padding: "16px",
				"@media (min-width: 600px)": {
					minHeight: "48px",
				},
			},
		},
		typography: themeTypography(themeOption),
	});

	themeConfiguration.components = componentStyleOverride(themeOption);

	return themeConfiguration;
};
