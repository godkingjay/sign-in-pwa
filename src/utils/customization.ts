const customization = {
	basename: "/",
	defaultPath: "/overview",
	borderRadius: 12,
};

export default customization;
