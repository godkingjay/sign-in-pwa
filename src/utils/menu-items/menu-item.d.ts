import { IconType } from "react-icons/lib";

export type MenuItemType = {
	id: string;
	title: string;
	type: "item" | "group" | "collapse";
	caption?: string;
	url?: string;
	target?: boolean;
	external?: boolean;
	icon?: IconType;
	disabled?: boolean;
	children?: MenuItemType[];
};
