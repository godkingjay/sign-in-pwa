import GettingStartedItems from "./getting-started";
import UtilitiesItems from "./utilities";
import { MenuItemType } from "./menu-item";

const MenuItems: MenuItemType[] = [GettingStartedItems, UtilitiesItems];

export default MenuItems;
