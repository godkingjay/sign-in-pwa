import {
	IoColorPaletteOutline,
	IoExtensionPuzzleOutline,
} from "react-icons/io5";
import { MenuItemType } from "./menu-item";

const UtilitiesItems: MenuItemType = {
	id: "utilities",
	title: "Utilities",
	caption: "Configuration",
	type: "group",
	children: [
		{
			id: "themes",
			title: "Themes",
			type: "item",
			url: "#themes",
			disabled: false,
			icon: IoColorPaletteOutline,
		},
		{
			id: "extensions",
			title: "Extensions",
			type: "item",
			url: "#extensions",
			disabled: false,
			icon: IoExtensionPuzzleOutline,
		},
	],
};

export default UtilitiesItems;
