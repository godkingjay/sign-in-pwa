import { IoRocketOutline } from "react-icons/io5";
import { HiOutlineArrowDownCircle, HiOutlineBookOpen } from "react-icons/hi2";
import { MenuItemType } from "./menu-item";

const GettingStartedItems: MenuItemType = {
	id: "getting-started",
	title: "Getting Started",
	caption: "Start Here",
	type: "group",
	children: [
		{
			id: "overview",
			title: "Overview",
			type: "item",
			url: "/overview",
			disabled: false,
			icon: IoRocketOutline,
		},
		{
			id: "documentation",
			title: "Documentation",
			type: "item",
			url: "#documentation",
			disabled: false,
			icon: HiOutlineBookOpen,
		},
		{
			id: "installation",
			title: "Installation",
			type: "item",
			url: "#installation",
			disabled: false,
			icon: HiOutlineArrowDownCircle,
		},
	],
};

export default GettingStartedItems;
