import { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
	appId: "com.sign_in.pwa.app",
	appName: "sign-in-pwa",
	webDir: "public",
	server: {
		url: "http://192.168.26.147:3000",
		cleartext: true,
	},
};

export default config;
