import million from "million/compiler";

/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	swcMinify: true,
	output: "export",
	experimental: {
		appDir: true,
	},
};

export default million.next(nextConfig);
